set project     "enzian_app_dcs"
set project_dir ${project}
set part        "xcvu9p-flgb2104-3-e"
set top_module  "enzian_app_dcs"
#set board_part  "eth.ch:enzian:1.0"

# IP location must be passed as the first argument.
set ip_dir [lindex $argv 0]

# Source files are included relative to the directory containing this script.
set src_dir   [file normalize "[file dirname [info script]]"]
set build_dir [file normalize "."]

# Source files for DCS
set dcs_src_dir "${src_dir}/directory-controller-slice"

# Source files for FPGA threads
set ft_src_dir "${src_dir}/fpga-thread-read-modify-write/ft_cnc_rmw"

# Create project
create_project $project "${build_dir}/${project}" -part $part
set proj [current_project]

# Set project properties
#set_property "board_part" $board_part                   $proj
set_property "default_lib" "xil_defaultlib"                 $proj
set_property "ip_cache_permissions" "read write"            $proj
set_property "ip_output_repo" "${build_dir}/${project}/${project}.cache/ip"  $proj
set_property "sim.ip.auto_export_scripts" "1"               $proj
set_property "simulator_language" "Mixed"                   $proj
set_property "target_language" "VHDL"                       $proj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY"           $proj
set_property "ip_repo_paths" "${ip_dir}"                      $proj
set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value {-mode out_of_context} -objects [get_runs synth_1]

# Make sure any repository IP is visible.
update_ip_catalog

#add_files -fileset [get_filesets sources_1] "${src_dir}/hdl" # Top added later. 
add_files -fileset [get_filesets sources_1] "${src_dir}/eci-toolkit/hdl"
add_files -fileset [get_filesets constrs_1] "${src_dir}/eci-toolkit/xdc"
set_property used_in_implementation false  [get_files -of_objects [get_filesets constrs_1]]

# Add DCS sources.
# eci_cmd_defs is got from eci_toolkit.
add_files -norecurse "$dcs_src_dir/dcs/dcu/eci_dirc_defs/eci_cc_defs.sv" \
    "$dcs_src_dir/dcs/eci_dcs_defs/rtl/eci_dcs_defs.sv" \
    "$dcs_src_dir/dcs/dcu/eci_dirc_defs/eci_dirc_defs.sv" \
    "$dcs_src_dir/dcs/dcu/arb_4_ecih/rtl/arb_4_ecih.sv" \
    "$dcs_src_dir/dcs/dcu/arb_4_ecih/rtl/arb_3_ecih.sv" \
    "$dcs_src_dir/dcs/dcu/arb_4_ecih/rtl/arb_2_ecih.sv" \
    "$dcs_src_dir/common/axis_comb_rr_arb/rtl/axis_comb_rr_arb.sv" \
    "$dcs_src_dir/common/axis_comb_router/rtl/axis_comb_router.sv" \
    "$dcs_src_dir/common/axis_pipeline_stage/rtl/axis_pipeline_stage.sv" \
    "$dcs_src_dir/common/vr_pipe_stage/rtl/vr_pipe_stage.sv" \
    "$dcs_src_dir/dcs/dcs_data_path/dp_data_store/rtl/dp_data_store.sv" \
    "$dcs_src_dir/dcs/dcs_data_path/dp_gate/rtl/dp_gate.sv" \
    "$dcs_src_dir/dcs/dcs_data_path/dp_gen_path/rtl/dp_gen_path.sv" \
    "$dcs_src_dir/dcs/dcs_data_path/dp_mem/rtl/dp_mem.sv" \
    "$dcs_src_dir/dcs/dcs_data_path/map_ecid_to_wrd/rtl/map_ecid_to_wrd.sv" \
    "$dcs_src_dir/dcs/dcs_data_path/dp_wr_ser/rtl/dp_wr_ser.sv" \
    "$dcs_src_dir/dcs/dcs_data_path/rd_data_path/rtl/rd_data_path.sv" \
    "$dcs_src_dir/dcs/dcs_data_path/wr_data_path/rtl/wr_data_path.sv" \
    "$dcs_src_dir/dcs/dcs_rr_arb/rtl/dcs_rr_arb.sv" \
    "$dcs_src_dir/dcs/dc_to_vc_router/rtl/dc_to_vc_router.sv" \
    "$dcs_src_dir/dcs/dcu/dcu_controller/rtl/dcu_controller.sv" \
    "$dcs_src_dir/dcs/dcu/dcu_tsu/rtl/dcu_tsu.sv" \
    "$dcs_src_dir/dcs/dcu/decode_eci_req/rtl/decode_eci_req.sv" \
    "$dcs_src_dir/dcs/dcu/eci_cc_table/rtl/eci_cc_table.sv" \
    "$dcs_src_dir/dcs/dcu/gen_out_header/rtl/gen_out_header.sv" \
    "$dcs_src_dir/dcs/dcu/ram_tdp/rtl/ram_tdp.sv" \
    "$dcs_src_dir/dcs/dcu/rd_trmgr/rtl/rd_trmgr.sv" \
    "$dcs_src_dir/dcs/dcu/eci_trmgr/rtl/eci_trmgr.sv" \
    "$dcs_src_dir/dcs/dcu/tag_state_ram/rtl/tag_state_ram.sv" \
    "$dcs_src_dir/dcs/dcu/wr_trmgr/rtl/wr_trmgr.sv" \
    "$dcs_src_dir/dcs/dcu/dcu/rtl/dcu.sv" \
    "$dcs_src_dir/dcs/dcu/dcu_top/rtl/dcu_top.sv" \
    "$dcs_src_dir/dcs/dcs_dcus/rtl/dcs_dcus.sv" \
    "$dcs_src_dir/dcs/dcs/rtl/dcs.sv" \
    "$dcs_src_dir/dcs/perf_sim_modules/perf_gen_seq_aliased/rtl/perf_gen_seq_aliased.sv" \
    "$dcs_src_dir/dcs/perf_sim_modules/cli_tput_load_gen/rtl/cli_tput_load_gen.sv" \
    "$dcs_src_dir/dcs/perf_sim_modules/cli_lat_load_gen/rtl/cli_lat_load_gen.sv" \
    "$dcs_src_dir/common/axis_xpm_fifo/rtl/axis_xpm_fifo.sv"

# Add DCS descriptors to AXI converter modules.
add_files -norecurse "$dcs_src_dir/desc_to_axi/axi_rd_cl/rtl/axi_rd_cl.sv" \
    "$dcs_src_dir/desc_to_axi/axi_wr_cl/rtl/axi_wr_cl.sv"

# Add FPGA thread sources.
add_files -norecurse "$ft_src_dir/ft_cnc_rmw_top/rtl/ft_cnc_rmw_top.sv" \
    "$ft_src_dir/ft_cnc_rmw/rtl/ft_cnc_rmw.sv" \
    "$ft_src_dir/gen_seq_al/rtl/gen_seq_al.sv" \
    "$ft_src_dir/ft_dc_desc_arb/rtl/ft_dc_desc_arb.sv" \
    "$ft_src_dir/f_rmw_thread/rtl/f_rmw_thread.sv" \
    "$ft_src_dir/ft_cnc_ctrl/rtl/ft_cnc_ctrl.sv" \
    "$dcs_src_dir/common/axis_2_router/rtl/axis_2_router.sv"

# Add Top sources.
add_files -norecurse "$src_dir/hdl/dcs/dcs_2_axi.sv" \
    "$src_dir/hdl/dcs/enzian_app_dcs.vhd"


# Set top entity
set_property "top" "${top_module}" [get_filesets sources_1]

# Create a project-local constraint file to take debugging constraints that we
# don't want to propagate to the repository.
file mkdir "${build_dir}/${project}/${project}.srcs/constrs_1"
close [ open "${build_dir}/${project}/${project}.srcs/constrs_1/local.xdc" w ]

### Regenerate IP
puts "regenerate IPs"
source "${src_dir}/eci-toolkit/create_ips.tcl"

# Regenerate block design
source "${src_dir}/bd/design_1.tcl"

# AXI BRAM controller for ECI DCS Byte addressable mem.
create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_0
set_property -dict { 
  CONFIG.DATA_WIDTH {512}
  CONFIG.ID_WIDTH {7}
  CONFIG.SUPPORTS_NARROW_BURST {0}
  CONFIG.SINGLE_PORT_BRAM {0}
  CONFIG.ECC_TYPE {0}
  CONFIG.BMG_INSTANCE {INTERNAL}
  CONFIG.MEM_DEPTH {1024}
  CONFIG.READ_LATENCY {1}
} [get_ips axi_bram_ctrl_0]
generate_target all [get_ips axi_bram_ctrl_0]

# Adding a vio to init the FPGA threads.
# create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_0
# set_property -dict { \
#   CONFIG.C_PROBE_OUT1_WIDTH {38} \
#   CONFIG.C_PROBE_OUT0_WIDTH {38} \
#   CONFIG.C_PROBE_IN7_WIDTH {38} \
#   CONFIG.C_PROBE_IN6_WIDTH {38} \
#   CONFIG.C_PROBE_IN5_WIDTH {38} \
#   CONFIG.C_PROBE_IN3_WIDTH {38} \
#   CONFIG.C_PROBE_IN2_WIDTH {38} \
#   CONFIG.C_PROBE_IN1_WIDTH {38} \
#   CONFIG.C_NUM_PROBE_OUT {3} \
#   CONFIG.C_NUM_PROBE_IN {8} \
# } [get_ips vio_0]
# generate_target all [get_ips vio_0]

close_project

# Update a local config file
set conffile [open "enzian.conf.tcl" "a"]
puts $conffile "set enzian_app \"${project}\""
puts $conffile "set project_dir \"${project_dir}\""
close $conffile
