----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 05/31/2022 01:05:08 PM
-- Design Name:
-- Module Name: Enzian App - stub - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity enzian_app_dcs is
generic(
  DDR_OR_BRAM  : integer := 0 --0 BRAM, 1 DDR.
  );
port (
-- 322.265625 MHz
    clk_sys                 : in std_logic;
-- 100MHz
    clk_io_out              : out std_logic;
-- programmed to 100MHz
    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
-- programmed to 300MHz
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;
-- power on reset
    reset_sys               : in std_logic;
-- ECI link status
    link1_up                : in std_logic;
    link2_up                : in std_logic;
-- ECI links
    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);
-- AXI Lite FPGA -> CPU
    m_io_axil_awaddr        : out std_logic_vector(43 downto 0);
    m_io_axil_awvalid       : out std_logic;
    m_io_axil_awready       : in std_logic;
    m_io_axil_wdata         : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb         : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid        : out std_logic;
    m_io_axil_wready        : in std_logic;
    m_io_axil_bresp         : in std_logic_vector(1 downto 0);
    m_io_axil_bvalid        : in std_logic;
    m_io_axil_bready        : out std_logic;
    m_io_axil_araddr        : out std_logic_vector(43 downto 0);
    m_io_axil_arvalid       : out std_logic;
    m_io_axil_arready       : in  std_logic;
    m_io_axil_rdata         : in std_logic_vector(63 downto 0);
    m_io_axil_rresp         : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid        : in std_logic;
    m_io_axil_rready        : out std_logic;
-- AXI Lite CPU -> FPGA
    s_io_axil_awaddr        : in std_logic_vector(43 downto 0);
    s_io_axil_awvalid       : in std_logic;
    s_io_axil_awready       : out std_logic;
    s_io_axil_wdata         : in std_logic_vector(63 downto 0);
    s_io_axil_wstrb         : in std_logic_vector(7 downto 0);
    s_io_axil_wvalid        : in std_logic;
    s_io_axil_wready        : out std_logic;
    s_io_axil_bresp         : out std_logic_vector(1 downto 0);
    s_io_axil_bvalid        : out std_logic;
    s_io_axil_bready        : in std_logic;
    s_io_axil_araddr        : in std_logic_vector(43 downto 0);
    s_io_axil_arvalid       : in std_logic;
    s_io_axil_arready       : out  std_logic;
    s_io_axil_rdata         : out std_logic_vector(63 downto 0);
    s_io_axil_rresp         : out std_logic_vector(1 downto 0);
    s_io_axil_rvalid        : out std_logic;
    s_io_axil_rready        : in std_logic;
-- BSCAN slave port for ILAs, VIOs, MIGs, MDMs etc.
    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;
-- DDR4
    F_D1_ACT_N : out std_logic;
    F_D1_A : out std_logic_vector ( 17 downto 0 );
    F_D1_BA : out std_logic_vector ( 1 downto 0 );
    F_D1_BG : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D1_CKE : out std_logic_vector ( 1 downto 0 );
    F_D1_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D1_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D1_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D1_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D1_ODT : out std_logic_vector ( 1 downto 0 );
    F_D1_PARITY_N : out std_logic;
    F_D1_RESET_N : out std_logic;
    F_D1C_CLK_N : in std_logic;
    F_D1C_CLK_P : in std_logic;

    F_D2_ACT_N : out std_logic;
    F_D2_A : out std_logic_vector ( 17 downto 0 );
    F_D2_BA : out std_logic_vector ( 1 downto 0 );
    F_D2_BG : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D2_CKE : out std_logic_vector ( 1 downto 0 );
    F_D2_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D2_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D2_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D2_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D2_ODT : out std_logic_vector ( 1 downto 0 );
    F_D2_PARITY_N : out std_logic;
    F_D2_RESET_N : out std_logic;
    F_D2C_CLK_N : in std_logic;
    F_D2C_CLK_P : in std_logic;

    F_D3_ACT_N : out std_logic;
    F_D3_A : out std_logic_vector ( 17 downto 0 );
    F_D3_BA : out std_logic_vector ( 1 downto 0 );
    F_D3_BG : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D3_CKE : out std_logic_vector ( 1 downto 0 );
    F_D3_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D3_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D3_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D3_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D3_ODT : out std_logic_vector ( 1 downto 0 );
    F_D3_PARITY_N : out std_logic;
    F_D3_RESET_N : out std_logic;
    F_D3C_CLK_N : in std_logic;
    F_D3C_CLK_P : in std_logic;

    F_D4_ACT_N : out std_logic;
    F_D4_A : out std_logic_vector ( 17 downto 0 );
    F_D4_BA : out std_logic_vector ( 1 downto 0 );
    F_D4_BG : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D4_CKE : out std_logic_vector ( 1 downto 0 );
    F_D4_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D4_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D4_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D4_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D4_ODT : out std_logic_vector ( 1 downto 0 );
    F_D4_PARITY_N : out std_logic;
    F_D4_RESET_N : out std_logic;
    F_D4C_CLK_N : in std_logic;
    F_D4C_CLK_P : in std_logic;
-- CMAC
    F_MAC0C_CLK_P   : in std_logic;
    F_MAC0C_CLK_N   : in std_logic;
    F_MAC0_TX_P : out std_logic_vector(3 downto 0);
    F_MAC0_TX_N : out std_logic_vector(3 downto 0);
    F_MAC0_RX_P : in std_logic_vector(3 downto 0);
    F_MAC0_RX_N : in std_logic_vector(3 downto 0);

    F_MAC1C_CLK_P   : in std_logic;
    F_MAC1C_CLK_N   : in std_logic;
    F_MAC1_TX_P : out std_logic_vector(3 downto 0);
    F_MAC1_TX_N : out std_logic_vector(3 downto 0);
    F_MAC1_RX_P : in std_logic_vector(3 downto 0);
    F_MAC1_RX_N : in std_logic_vector(3 downto 0);

    F_MAC2C_CLK_P   : in std_logic;
    F_MAC2C_CLK_N   : in std_logic;
    F_MAC2_TX_P : out std_logic_vector(3 downto 0);
    F_MAC2_TX_N : out std_logic_vector(3 downto 0);
    F_MAC2_RX_P : in std_logic_vector(3 downto 0);
    F_MAC2_RX_N : in std_logic_vector(3 downto 0);

    F_MAC3C_CLK_P   : in std_logic;
    F_MAC3C_CLK_N   : in std_logic;
    F_MAC3_TX_P : out std_logic_vector(3 downto 0);
    F_MAC3_TX_N : out std_logic_vector(3 downto 0);
    F_MAC3_RX_P : in std_logic_vector(3 downto 0);
    F_MAC3_RX_N : in std_logic_vector(3 downto 0);
-- PCIE x16
    F_PCIE16C_CLK_P   : in std_logic;
    F_PCIE16C_CLK_N   : in std_logic;
    F_PCIE16_TX_P : out std_logic_vector(15 downto 0);
    F_PCIE16_TX_N : out std_logic_vector(15 downto 0);
    F_PCIE16_RX_P : in std_logic_vector(15 downto 0);
    F_PCIE16_RX_N : in std_logic_vector(15 downto 0);
-- NVMe
    F_NVMEC_CLK_P   : in std_logic;
    F_NVMEC_CLK_N   : in std_logic;
    F_NVME_TX_P : out std_logic_vector(3 downto 0);
    F_NVME_TX_N : out std_logic_vector(3 downto 0);
    F_NVME_RX_P : in std_logic_vector(3 downto 0);
    F_NVME_RX_N : in std_logic_vector(3 downto 0);
-- C2C
    B_C2CC_CLK_P    : in std_logic;
    B_C2CC_CLK_N    : in std_logic;
    B_C2C_TX_P      : in std_logic_vector(0 downto 0);
    B_C2C_TX_N      : in std_logic_vector(0 downto 0);
    B_C2C_RX_P      : out std_logic_vector(0 downto 0);
    B_C2C_RX_N      : out std_logic_vector(0 downto 0);
    B_C2C_NMI       : in std_logic;
-- I2C
    F_I2C0_SDA      : inout std_logic;
    F_I2C0_SCL      : out std_logic;

    F_I2C1_SDA      : inout std_logic;
    F_I2C1_SCL      : out std_logic;

    F_I2C2_SDA      : inout std_logic;
    F_I2C2_SCL      : out std_logic;

    F_I2C3_SDA      : inout std_logic;
    F_I2C3_SCL      : out std_logic;

    F_I2C4_SDA      : inout std_logic;
    F_I2C4_SCL      : out std_logic;

    F_I2C5_SDA      : inout std_logic;
    F_I2C5_SCL      : out std_logic;
    F_I2C5_RESET_N  : out std_logic;
    F_I2C5_INT_N    : in std_logic;
-- FUART
    B_FUART_TXD     : in std_logic;
    B_FUART_RXD     : out std_logic;
    B_FUART_RTS     : in std_logic;
    B_FUART_CTS     : out std_logic;
-- IRQ
    F_IRQ_IRQ0      : out std_logic;
    F_IRQ_IRQ1      : out std_logic;
    F_IRQ_IRQ2      : out std_logic;
    F_IRQ_IRQ3      : out std_logic
);
end enzian_app_dcs;

architecture Behavioral of enzian_app_dcs is

component eci_gateway is
generic (
    TX_NO_CHANNELS      : integer;
    RX_NO_CHANNELS      : integer;
    RX_FILTER_VC        : VC_BITFIELDS;
    RX_FILTER_TYPE_MASK : ECI_TYPE_MASKS;
    RX_FILTER_TYPE      : ECI_TYPE_MASKS;
    RX_FILTER_CLI_MASK  : CLI_ARRAY;
    RX_FILTER_CLI       : CLI_ARRAY
);
port (
    clk_sys                 : in std_logic;
    clk_io_out              : out std_logic;
    clk_prgc0_out           : out std_logic;
    clk_prgc1_out           : out std_logic;

    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;

    reset_sys               : in std_logic;
    reset_out               : out std_logic;
    reset_n_out             : out std_logic;
    link1_up                : in std_logic;
    link2_up                : in std_logic;

    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);

    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;

    m0_bscan_bscanid_en     : out std_logic;
    m0_bscan_capture        : out std_logic;
    m0_bscan_drck           : out std_logic;
    m0_bscan_reset          : out std_logic;
    m0_bscan_runtest        : out std_logic;
    m0_bscan_sel            : out std_logic;
    m0_bscan_shift          : out std_logic;
    m0_bscan_tck            : out std_logic;
    m0_bscan_tdi            : out std_logic;
    m0_bscan_tdo            : in std_logic;
    m0_bscan_tms            : out std_logic;
    m0_bscan_update         : out std_logic;

    rx_eci_channels         : out ARRAY_ECI_CHANNELS(RX_NO_CHANNELS-1 downto 0);
    rx_eci_channels_ready   : in std_logic_vector(RX_NO_CHANNELS-1 downto 0);

    tx_eci_channels         : in ARRAY_ECI_CHANNELS(TX_NO_CHANNELS-1 downto 0);
    tx_eci_channels_ready   : out std_logic_vector(TX_NO_CHANNELS-1 downto 0)
);
end component;

component eci_channel_bus_converter is
port (
    clk             : in STD_LOGIC;

    in_channel      : in ECI_CHANNEL;
    in_ready        : out STD_LOGIC;

    out_data        : out WORDS(16 downto 0);
    out_vc_no       : out std_logic_vector(3 downto 0);
    out_size        : out std_logic_vector(4 downto 0);
    out_valid       : out std_logic;
    out_ready       : in std_logic
);
end component;

component eci_bus_channel_converter is
port (
    clk             : in STD_LOGIC;

    in_data         : in WORDS(16 downto 0);
    in_vc_no        : in std_logic_vector(3 downto 0);
    in_size         : in std_logic_vector(4 downto 0);
    in_valid        : in std_logic;
    in_ready        : out std_logic;

    out_channel     : out ECI_CHANNEL;
    out_ready       : in STD_LOGIC
);
end component;

component loopback_vc_resp_nodata is
generic (
   WORD_WIDTH : integer;
   GSDN_GSYNC_FN : integer
);
port (
    clk, reset : in std_logic;

    -- ECI Request input stream
    vc_req_i       : in  std_logic_vector(63 downto 0);
    vc_req_valid_i : in  std_logic;
    vc_req_ready_o : out std_logic;

    -- ECI Response output stream
    vc_resp_o       : out std_logic_vector(63 downto 0);
    vc_resp_valid_o : out std_logic;
    vc_resp_ready_i : in  std_logic
);
end component;

component dcs_2_axi is
  generic (
    FPGA_THREAD_ODD_GEN : std_logic
);
port (
  clk, reset : in std_logic;

  -- Input ECI events.
  -- ECI packet for request without data. (VC 6 or 7) (only header).
  req_wod_hdr_i       : in std_logic_vector(63 downto 0);
  req_wod_pkt_size_i  : in std_logic_vector( 4 downto 0);
  req_wod_pkt_vc_i    : in std_logic_vector( 3 downto 0);
  req_wod_pkt_valid_i : in std_logic;
  req_wod_pkt_ready_o : out std_logic;

  -- ECI packet for response without data.(VC 10 or 11). (only header).
  rsp_wod_hdr_i       : in std_logic_vector(63 downto 0);
  rsp_wod_pkt_size_i  : in std_logic_vector( 4 downto 0);
  rsp_wod_pkt_vc_i    : in std_logic_vector( 3 downto 0);
  rsp_wod_pkt_valid_i : in std_logic;
  rsp_wod_pkt_ready_o : out std_logic;

  -- ECI packet for response with data. (VC 4 or 5). (header + data).
  rsp_wd_pkt_i        : in std_logic_vector(17*64-1 downto 0);
  rsp_wd_pkt_size_i   : in std_logic_vector( 4 downto 0);
  rsp_wd_pkt_vc_i     : in std_logic_vector( 3 downto 0);
  rsp_wd_pkt_valid_i  : in std_logic;
  rsp_wd_pkt_ready_o  : out std_logic;

  -- Output ECI events. (rsp without data, rsp with data).
  -- VC 10,11
  rsp_wod_hdr_o       : out std_logic_vector(63 downto 0);
  rsp_wod_pkt_size_o  : out std_logic_vector( 4 downto 0);
  rsp_wod_pkt_vc_o    : out std_logic_vector( 3 downto 0);
  rsp_wod_pkt_valid_o : out std_logic;
  rsp_wod_pkt_ready_i : in std_logic;

  -- Responses with data (VC 5 or 4)
  -- header+payload
  rsp_wd_pkt_o       : out std_logic_vector(17*64-1 downto 0);
  rsp_wd_pkt_size_o  : out std_logic_vector( 4 downto 0);
  rsp_wd_pkt_vc_o    : out std_logic_vector( 3 downto 0);
  rsp_wd_pkt_valid_o : out std_logic;
  rsp_wd_pkt_ready_i : in std_logic;

  -- forwards without data (VC 8 or 9).
  fwd_wod_hdr_o       : out std_logic_vector(63 downto 0);
  fwd_wod_pkt_size_o  : out std_logic_vector( 4 downto 0);
  fwd_wod_pkt_vc_o    : out std_logic_vector( 3 downto 0);
  fwd_wod_pkt_valid_o : out std_logic;
  fwd_wod_pkt_ready_i : in std_logic;

  -- FPGA thread signals. 
  f_st_un_al_addr_i   : in std_logic_vector(37 downto 0);
  f_num_cls_to_rmw_i  : in std_logic_vector(37 downto 0);
  f_rmw_en_i          : in std_logic;
  f_rmw_done_o        : out std_logic;

  -- FPGA thread perf counters.
  f_tot_timer_o     : out std_logic_vector(37 downto 0);
  f_num_wr_issued_o : out std_logic_vector(37 downto 0);
  f_num_rd_issued_o : out std_logic_vector(37 downto 0);
  
  -- Primary AXI rd/wr i/f.
  p_axi_arid    : out std_logic_vector( 6 downto 0);
  p_axi_araddr  : out std_logic_vector(37 downto 0);
  p_axi_arlen   : out std_logic_vector( 7 downto 0);
  p_axi_arsize  : out std_logic_vector( 2 downto 0);
  p_axi_arburst : out std_logic_vector( 1 downto 0);
  p_axi_arlock  : out std_logic;
  p_axi_arcache : out std_logic_vector( 3 downto 0);
  p_axi_arprot  : out std_logic_vector( 2 downto 0);
  p_axi_arvalid : out std_logic;
  p_axi_arready : in std_logic;
  p_axi_rid     : in std_logic_vector( 6 downto 0);
  p_axi_rdata   : in std_logic_vector(511 downto 0);
  p_axi_rresp   : in std_logic_vector( 1 downto 0);
  p_axi_rlast   : in std_logic;
  p_axi_rvalid  : in std_logic;
  p_axi_rready  : out std_logic;

  p_axi_awid    : out std_logic_vector ( 6 downto 0);
  p_axi_awaddr  : out std_logic_vector (37 downto 0);
  p_axi_awlen   : out std_logic_vector ( 7 downto 0);
  p_axi_awsize  : out std_logic_vector ( 2 downto 0);
  p_axi_awburst : out std_logic_vector ( 1 downto 0);
  p_axi_awlock  : out std_logic;
  p_axi_awcache : out std_logic_vector ( 3 downto 0);
  p_axi_awprot  : out std_logic_vector ( 2 downto 0);
  p_axi_awvalid : out std_logic;
  p_axi_awready : in std_logic ;
  p_axi_wdata   : out std_logic_vector (511 downto 0);
  p_axi_wstrb   : out std_logic_vector (63 downto 0);
  p_axi_wlast   : out std_logic;
  p_axi_wvalid  : out std_logic;
  p_axi_wready  : in std_logic;
  p_axi_bid     : in std_logic_vector( 6 downto 0);
  p_axi_bresp   : in std_logic_vector( 1 downto 0);
  p_axi_bvalid  : in std_logic;
  p_axi_bready  : out std_logic
);
end component;

-- DDR block design.
component  design_1 is
  port (
    clk_sys : in STD_LOGIC;

    ddr4_c1_axi_araddr : in STD_LOGIC_VECTOR ( 37 downto 0 );
    ddr4_c1_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_arid : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c1_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ddr4_c1_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c1_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_arready : out STD_LOGIC;
    ddr4_c1_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c1_axi_arvalid : in STD_LOGIC;
    ddr4_c1_axi_awaddr : in STD_LOGIC_VECTOR ( 37 downto 0 );
    ddr4_c1_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_awid : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c1_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ddr4_c1_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c1_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_awready : out STD_LOGIC;
    ddr4_c1_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c1_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c1_axi_awvalid : in STD_LOGIC;
    ddr4_c1_axi_bid : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c1_axi_bready : in STD_LOGIC;
    ddr4_c1_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_bvalid : out STD_LOGIC;
    ddr4_c1_axi_ctrl_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c1_axi_ctrl_arready : out STD_LOGIC;
    ddr4_c1_axi_ctrl_arvalid : in STD_LOGIC;
    ddr4_c1_axi_ctrl_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c1_axi_ctrl_awready : out STD_LOGIC;
    ddr4_c1_axi_ctrl_awvalid : in STD_LOGIC;
    ddr4_c1_axi_ctrl_bready : in STD_LOGIC;
    ddr4_c1_axi_ctrl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_ctrl_bvalid : out STD_LOGIC;
    ddr4_c1_axi_ctrl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c1_axi_ctrl_rready : in STD_LOGIC;
    ddr4_c1_axi_ctrl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_ctrl_rvalid : out STD_LOGIC;
    ddr4_c1_axi_ctrl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c1_axi_ctrl_wready : out STD_LOGIC;
    ddr4_c1_axi_ctrl_wvalid : in STD_LOGIC;
    ddr4_c1_axi_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    ddr4_c1_axi_rid : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c1_axi_rlast : out STD_LOGIC;
    ddr4_c1_axi_rready : in STD_LOGIC;
    ddr4_c1_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_axi_rvalid : out STD_LOGIC;
    ddr4_c1_axi_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    ddr4_c1_axi_wlast : in STD_LOGIC;
    ddr4_c1_axi_wready : out STD_LOGIC;
    ddr4_c1_axi_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ddr4_c1_axi_wvalid : in STD_LOGIC;

    ddr4_c1_act_n : out STD_LOGIC;
    ddr4_c1_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
    ddr4_c1_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c1_ck_c : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_ck_t : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_cs_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
    ddr4_c1_dqs_c : inout STD_LOGIC_VECTOR ( 17 downto 0 );
    ddr4_c1_dqs_t : inout STD_LOGIC_VECTOR ( 17 downto 0 );
    ddr4_c1_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c1_par : out STD_LOGIC;
    ddr4_c1_reset_n : out STD_LOGIC;
    ddr4_c1_sys_clk_n : in STD_LOGIC;
    ddr4_c1_sys_clk_p : in STD_LOGIC;
    ddr4_c1_ui_clk : out STD_LOGIC;

    ddr4_c4_axi_araddr : in STD_LOGIC_VECTOR ( 37 downto 0 );
    ddr4_c4_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_arid : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c4_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ddr4_c4_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c4_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_arready : out STD_LOGIC;
    ddr4_c4_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c4_axi_arvalid : in STD_LOGIC;
    ddr4_c4_axi_awaddr : in STD_LOGIC_VECTOR ( 37 downto 0 );
    ddr4_c4_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_awid : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c4_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ddr4_c4_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c4_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_awready : out STD_LOGIC;
    ddr4_c4_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ddr4_c4_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    ddr4_c4_axi_awvalid : in STD_LOGIC;
    ddr4_c4_axi_bid : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c4_axi_bready : in STD_LOGIC;
    ddr4_c4_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_bvalid : out STD_LOGIC;
    ddr4_c4_axi_ctrl_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c4_axi_ctrl_arready : out STD_LOGIC;
    ddr4_c4_axi_ctrl_arvalid : in STD_LOGIC;
    ddr4_c4_axi_ctrl_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c4_axi_ctrl_awready : out STD_LOGIC;
    ddr4_c4_axi_ctrl_awvalid : in STD_LOGIC;
    ddr4_c4_axi_ctrl_bready : in STD_LOGIC;
    ddr4_c4_axi_ctrl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_ctrl_bvalid : out STD_LOGIC;
    ddr4_c4_axi_ctrl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c4_axi_ctrl_rready : in STD_LOGIC;
    ddr4_c4_axi_ctrl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_ctrl_rvalid : out STD_LOGIC;
    ddr4_c4_axi_ctrl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ddr4_c4_axi_ctrl_wready : out STD_LOGIC;
    ddr4_c4_axi_ctrl_wvalid : in STD_LOGIC;
    ddr4_c4_axi_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    ddr4_c4_axi_rid : out STD_LOGIC_VECTOR ( 6 downto 0 );
    ddr4_c4_axi_rlast : out STD_LOGIC;
    ddr4_c4_axi_rready : in STD_LOGIC;
    ddr4_c4_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_axi_rvalid : out STD_LOGIC;
    ddr4_c4_axi_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    ddr4_c4_axi_wlast : in STD_LOGIC;
    ddr4_c4_axi_wready : out STD_LOGIC;
    ddr4_c4_axi_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ddr4_c4_axi_wvalid : in STD_LOGIC;

    ddr4_c4_act_n : out STD_LOGIC;
    ddr4_c4_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
    ddr4_c4_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_c4_ck_c : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_ck_t : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_cs_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
    ddr4_c4_dqs_c : inout STD_LOGIC_VECTOR ( 17 downto 0 );
    ddr4_c4_dqs_t : inout STD_LOGIC_VECTOR ( 17 downto 0 );
    ddr4_c4_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_c4_par : out STD_LOGIC;
    ddr4_c4_reset_n : out STD_LOGIC;
    ddr4_c4_sys_clk_n : in STD_LOGIC;
    ddr4_c4_sys_clk_p : in STD_LOGIC;
    ddr4_c4_ui_clk : out STD_LOGIC;
    ddr4_sys_rst : in STD_LOGIC;
    reset_n : in STD_LOGIC
  );
end component;

-- BRAM to connect to DC slices.
component axi_bram_ctrl_0 is
port (
    s_axi_aclk    : IN STD_LOGIC;
    s_axi_aresetn : IN STD_LOGIC;
    s_axi_awid    : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    s_axi_awaddr  : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axi_awlen   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_awsize  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_awlock  : IN STD_LOGIC;
    s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_awprot  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awvalid : IN STD_LOGIC;
    s_axi_awready : OUT STD_LOGIC;
    s_axi_wdata   : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axi_wstrb   : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axi_wlast   : IN STD_LOGIC;
    s_axi_wvalid  : IN STD_LOGIC;
    s_axi_wready  : OUT STD_LOGIC;
    s_axi_bid     : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    s_axi_bresp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_bvalid  : OUT STD_LOGIC;
    s_axi_bready  : IN STD_LOGIC;
    s_axi_arid    : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    s_axi_araddr  : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axi_arlen   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axi_arsize  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_arlock  : IN STD_LOGIC;
    s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_arprot  : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arvalid : IN STD_LOGIC;
    s_axi_arready : OUT STD_LOGIC;
    s_axi_rid     : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    s_axi_rdata   : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axi_rresp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_rlast   : OUT STD_LOGIC;
    s_axi_rvalid  : OUT STD_LOGIC;
    s_axi_rready  : IN STD_LOGIC
  );
end component;

-- VIO to init the FPGA threads and
-- observe performance counters. 
-- component vio_0 is
--   port (
--     clk : in std_logic;
--     -- input probes.
--     probe_in0  : in std_logic_vector(0 downto 0);
--     probe_in1  : in std_logic_vector(37 downto 0);
--     probe_in2  : in std_logic_vector(37 downto 0);
--     probe_in3  : in std_logic_vector(37 downto 0);
--     probe_in4  : in std_logic_vector(0 downto 0);
--     probe_in5  : in std_logic_vector(37 downto 0);
--     probe_in6  : in std_logic_vector(37 downto 0);
--     probe_in7  : in std_logic_vector(37 downto 0);
--     -- output probes.
--     probe_out0 :  out std_logic_vector(37 downto 0);
--     probe_out1 :  out std_logic_vector(37 downto 0);
--     probe_out2 :  out std_logic_vector(0 downto 0)
--     );
-- end component;

component ft_cnc_ctrl is
  port (
    clk : in std_logic;
    reset : in std_logic;
    s_io_axil_awaddr        : in std_logic_vector(43 downto 0);
    s_io_axil_awvalid       : in std_logic;
    s_io_axil_awready       : out std_logic;
    s_io_axil_wdata         : in std_logic_vector(63 downto 0);
    s_io_axil_wstrb         : in std_logic_vector(7 downto 0);
    s_io_axil_wvalid        : in std_logic;
    s_io_axil_wready        : out std_logic;
    s_io_axil_bresp         : out std_logic_vector(1 downto 0);
    s_io_axil_bvalid        : out std_logic;
    s_io_axil_bready        : in std_logic;
    s_io_axil_araddr        : in std_logic_vector(43 downto 0);
    s_io_axil_arvalid       : in std_logic;
    s_io_axil_arready       : out  std_logic;
    s_io_axil_rdata         : out std_logic_vector(63 downto 0);
    s_io_axil_rresp         : out std_logic_vector(1 downto 0);
    s_io_axil_rvalid        : out std_logic;
    s_io_axil_rready        : in std_logic;
    st_un_al_addr_o         : out std_logic_vector(37 downto 0);
    num_cls_to_rmw_o        : out std_logic_vector(37 downto 0);
    rmw_en_o                : out std_logic;
    odd_tot_timer_i         : in std_logic_vector(37 downto 0);
    odd_num_wr_issued_i     : in std_logic_vector(37 downto 0);
    odd_num_rd_issued_i     : in std_logic_vector(37 downto 0);
    even_tot_timer_i        : in std_logic_vector(37 downto 0);
    even_num_wr_issued_i    : in std_logic_vector(37 downto 0);
    even_num_rd_issued_i    : in std_logic_vector(37 downto 0)
    );
end component;
  
type ECI_PACKET_RX is record
    c6_gsync            : ECI_CHANNEL;
    c6_gsync_ready      : std_logic;
    c7_gsync            : ECI_CHANNEL;
    c7_gsync_ready      : std_logic;
    ginv                : ECI_CHANNEL;
    -- RX req_wod VC 6,7
    dcs_c6              : ECI_CHANNEL;
    dcs_c6_ready        : std_logic;
    dcs_c7              : ECI_CHANNEL;
    dcs_c7_ready        : std_logic;
    -- RX rsp_wod VC 10,11
    dcs_c10             : ECI_CHANNEL;
    dcs_c10_ready       : std_logic;
    dcs_c11             : ECI_CHANNEL;
    dcs_c11_ready       : std_logic;
    -- RX rsp_wd VC 4,5
    dcs_c4              : ECI_CHANNEL;
    dcs_c4_ready        : std_logic;
    dcs_c5              : ECI_CHANNEL;
    dcs_c5_ready        : std_logic;
    -- RX rsp_wd VC 4,5 ECI packet.
    dcs_c4_wd_pkt       : WORDS(16 downto 0);
    dcs_c4_wd_pkt_size  : std_logic_vector(4 downto 0);
    dcs_c4_wd_pkt_vc    : std_logic_vector(3 downto 0);
    dcs_c4_wd_pkt_valid : std_logic;
    dcs_c4_wd_pkt_ready : std_logic;
    dcs_c5_wd_pkt       : WORDS(16 downto 0);
    dcs_c5_wd_pkt_size  : std_logic_vector(4 downto 0);
    dcs_c5_wd_pkt_vc    : std_logic_vector(3 downto 0);
    dcs_c5_wd_pkt_valid : std_logic;
    dcs_c5_wd_pkt_ready : std_logic;
end record ECI_PACKET_RX;

type ECI_PACKET_TX is record
    c10_gsync           : ECI_CHANNEL;
    c10_gsync_ready     : std_logic;
    c11_gsync           : ECI_CHANNEL;
    c11_gsync_ready     : std_logic;
    -- TX rsp_wod VC 10,11
    dcs_c10             : ECI_CHANNEL;
    dcs_c10_ready       : std_logic;
    dcs_c11             : ECI_CHANNEL;
    dcs_c11_ready       : std_logic;
    -- TX rsp_wd VC 4,5
    dcs_c4              : ECI_CHANNEL;
    dcs_c4_ready        : std_logic;
    dcs_c5              : ECI_CHANNEL;
    dcs_c5_ready        : std_logic;
    -- TX rsp_wd VC 4,5 ECI packet.
    dcs_c4_wd_pkt       : std_logic_vector(17*64-1 downto 0);
    dcs_c4_wd_pkt_size  : std_logic_vector(4 downto 0);
    dcs_c4_wd_pkt_vc    : std_logic_vector(3 downto 0);
    dcs_c4_wd_pkt_valid : std_logic;
    dcs_c4_wd_pkt_ready : std_logic;
    dcs_c5_wd_pkt       : std_logic_vector(17*64-1 downto 0);
    dcs_c5_wd_pkt_size  : std_logic_vector(4 downto 0);
    dcs_c5_wd_pkt_vc    : std_logic_vector(3 downto 0);
    dcs_c5_wd_pkt_valid : std_logic;
    dcs_c5_wd_pkt_ready : std_logic;
    -- TX fwd_wod VC 8.9
    dcs_c8              : ECI_CHANNEL;
    dcs_c8_ready        : std_logic;
    dcs_c9              : ECI_CHANNEL;
    dcs_c9_ready        : std_logic;
end record ECI_PACKET_TX;

type DCS_AXI is record
    arid    : std_logic_vector( 6 downto 0);
    araddr  : std_logic_vector(37 downto 0);
    arlen   : std_logic_vector( 7 downto 0);
    arsize  : std_logic_vector( 2 downto 0);
    arburst : std_logic_vector( 1 downto 0);
    arlock  : std_logic;
    arcache : std_logic_vector( 3 downto 0);
    arprot  : std_logic_vector( 2 downto 0);
    arvalid : std_logic;
    arready : std_logic;
    rid     : std_logic_vector( 6 downto 0);
    rdata   : std_logic_vector(511 downto 0);
    rresp   : std_logic_vector( 1 downto 0);
    rlast   : std_logic;
    rvalid  : std_logic;
    rready  : std_logic;

    awid    : std_logic_vector ( 6 downto 0);
    awaddr  : std_logic_vector (37 downto 0);
    awlen   : std_logic_vector ( 7 downto 0);
    awsize  : std_logic_vector ( 2 downto 0);
    awburst : std_logic_vector ( 1 downto 0);
    awlock  : std_logic;
    awcache : std_logic_vector ( 3 downto 0);
    awprot  : std_logic_vector ( 2 downto 0);
    awvalid : std_logic;
    awready : std_logic ;
    wdata   : std_logic_vector (511 downto 0);
    wstrb   : std_logic_vector (63 downto 0);
    wlast   : std_logic;
    wvalid  : std_logic;
    wready  : std_logic;
    bid     : std_logic_vector( 6 downto 0);
    bresp   : std_logic_vector( 1 downto 0);
    bvalid  : std_logic;
    bready  : std_logic;
end record DCS_AXI;

type BSCAN is record
    bscanid_en     : std_logic;
    capture        : std_logic;
    drck           : std_logic;
    reset          : std_logic;
    runtest        : std_logic;
    sel            : std_logic;
    shift          : std_logic;
    tck            : std_logic;
    tdi            : std_logic;
    tdo            : std_logic;
    tms            : std_logic;
    update         : std_logic;
end record BSCAN;

type LCL_CHANNEL is record
  data  : std_logic_vector(63 downto 0);
  size  : std_logic_vector(4 downto 0);
  vc_no : std_logic_vector(4 downto 0);
  valid : std_logic;
  ready : std_logic;
end record LCL_CHANNEL;

signal m0_bscan : BSCAN;

signal link_eci_packet_rx : ECI_PACKET_RX;
signal link_eci_packet_tx : ECI_PACKET_TX;

signal clk, clk_io : std_logic;
signal reset : std_logic;
signal reset_n : std_logic;

signal dcs_even_axi, dcs_odd_axi : DCS_AXI;

signal a, b, c, d : std_logic;

-- -- VIO signals
-- signal f_st_un_al_addr_i  : std_logic_vector(37 downto 0);
-- signal f_num_cls_to_rmw_i : std_logic_vector(37 downto 0);
-- signal f_rmw_en_i         : std_logic_vector(0 downto 0);
-- -- Perf counters from odd DCS to VIO.
-- signal do_f_rmw_done_o    : std_logic_vector(0 downto 0);
-- signal do_f_tot_timer_o   : std_logic_vector(37 downto 0);
-- signal do_f_num_wr_issued_o : std_logic_vector(37 downto 0);
-- signal do_f_num_rd_issued_o : std_logic_vector(37 downto 0);
-- -- Perf counters from even DCS to VIO.
-- signal de_f_rmw_done_o    : std_logic_vector(0 downto 0);
-- signal de_f_tot_timer_o   : std_logic_vector(37 downto 0);
-- signal de_f_num_wr_issued_o : std_logic_vector(37 downto 0);
-- signal de_f_num_rd_issued_o : std_logic_vector(37 downto 0);

-- AXIL signals
signal f_st_un_al_addr_i  : std_logic_vector(37 downto 0);
signal f_num_cls_to_rmw_i : std_logic_vector(37 downto 0);
signal f_rmw_en_i         : std_logic;
-- Perf counters from odd DCS to VIO.
signal do_f_rmw_done_o    : std_logic_vector(0 downto 0);
signal do_f_tot_timer_o   : std_logic_vector(37 downto 0);
signal do_f_num_wr_issued_o : std_logic_vector(37 downto 0);
signal do_f_num_rd_issued_o : std_logic_vector(37 downto 0);
-- Perf counters from even DCS to VIO.
signal de_f_rmw_done_o    : std_logic_vector(0 downto 0);
signal de_f_tot_timer_o   : std_logic_vector(37 downto 0);
signal de_f_num_wr_issued_o : std_logic_vector(37 downto 0);
signal de_f_num_rd_issued_o : std_logic_vector(37 downto 0);


begin

clk <= clk_sys;
clk_io_out <= clk_io;

i_eci_gateway : eci_gateway
  generic map (
    TX_NO_CHANNELS      => 8,
    RX_NO_CHANNELS      => 9,
    RX_FILTER_VC        => ("00000100000", "00000010000", "00000110000", "00000100000", "00000010000", "01000000000", "00100000000", "00000001000", "00000000100"),
    RX_FILTER_TYPE_MASK => ("11111", "11111", "11111", "00000", "00000", "00000", "00000", "00000", "00000"),
    RX_FILTER_TYPE      => ("11000", "11000", "10100", "00000", "00000", "00000", "00000", "00000", "00000"),
    RX_FILTER_CLI_MASK  => ((others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0')),
    RX_FILTER_CLI       => ((others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'), (others => '0'))
)
port map (
    clk_sys                 => clk,
    clk_io_out              => clk_io,

    prgc0_clk_p             => prgc0_clk_p,
    prgc0_clk_n             => prgc0_clk_n,
    prgc1_clk_p             => prgc1_clk_p,
    prgc1_clk_n             => prgc1_clk_n,

    reset_sys               => reset_sys,
    reset_out               => reset,
    reset_n_out             => reset_n,
    link1_up                => link1_up,
    link2_up                => link2_up,

    link1_in_data           => link1_in_data,
    link1_in_vc_no          => link1_in_vc_no,
    link1_in_we2            => link1_in_we2,
    link1_in_we3            => link1_in_we3,
    link1_in_we4            => link1_in_we4,
    link1_in_we5            => link1_in_we5,
    link1_in_valid          => link1_in_valid,
    link1_in_credit_return  => link1_in_credit_return,

    link1_out_hi_data       => link1_out_hi_data,
    link1_out_hi_vc_no      => link1_out_hi_vc_no,
    link1_out_hi_size       => link1_out_hi_size,
    link1_out_hi_valid      => link1_out_hi_valid,
    link1_out_hi_ready      => link1_out_hi_ready,

    link1_out_lo_data       => link1_out_lo_data,
    link1_out_lo_vc_no      => link1_out_lo_vc_no,
    link1_out_lo_valid      => link1_out_lo_valid,
    link1_out_lo_ready      => link1_out_lo_ready,
    link1_out_credit_return => link1_out_credit_return,

    link2_in_data           => link2_in_data,
    link2_in_vc_no          => link2_in_vc_no,
    link2_in_we2            => link2_in_we2,
    link2_in_we3            => link2_in_we3,
    link2_in_we4            => link2_in_we4,
    link2_in_we5            => link2_in_we5,
    link2_in_valid          => link2_in_valid,
    link2_in_credit_return  => link2_in_credit_return,

    link2_out_hi_data       => link2_out_hi_data,
    link2_out_hi_vc_no      => link2_out_hi_vc_no,
    link2_out_hi_size       => link2_out_hi_size,
    link2_out_hi_valid      => link2_out_hi_valid,
    link2_out_hi_ready      => link2_out_hi_ready,

    link2_out_lo_data       => link2_out_lo_data,
    link2_out_lo_vc_no      => link2_out_lo_vc_no,
    link2_out_lo_valid      => link2_out_lo_valid,
    link2_out_lo_ready      => link2_out_lo_ready,
    link2_out_credit_return => link2_out_credit_return,

    s_bscan_bscanid_en      => s_bscan_bscanid_en,
    s_bscan_capture         => s_bscan_capture,
    s_bscan_drck            => s_bscan_drck,
    s_bscan_reset           => s_bscan_reset,
    s_bscan_runtest         => s_bscan_runtest,
    s_bscan_sel             => s_bscan_sel,
    s_bscan_shift           => s_bscan_shift,
    s_bscan_tck             => s_bscan_tck,
    s_bscan_tdi             => s_bscan_tdi,
    s_bscan_tdo             => s_bscan_tdo,
    s_bscan_tms             => s_bscan_tms,
    s_bscan_update          => s_bscan_update,

    m0_bscan_bscanid_en     => m0_bscan.bscanid_en,
    m0_bscan_capture        => m0_bscan.capture,
    m0_bscan_drck           => m0_bscan.drck,
    m0_bscan_reset          => m0_bscan.reset,
    m0_bscan_runtest        => m0_bscan.runtest,
    m0_bscan_sel            => m0_bscan.sel,
    m0_bscan_shift          => m0_bscan.shift,
    m0_bscan_tck            => m0_bscan.tck,
    m0_bscan_tdi            => m0_bscan.tdi,
    m0_bscan_tdo            => m0_bscan.tdo,
    m0_bscan_tms            => m0_bscan.tms,
    m0_bscan_update         => m0_bscan.update,

    rx_eci_channels(0)      => link_eci_packet_rx.c7_gsync,
    rx_eci_channels(1)      => link_eci_packet_rx.c6_gsync,
    rx_eci_channels(2)      => link_eci_packet_rx.ginv,
    rx_eci_channels(3)      => link_eci_packet_rx.dcs_c7,
    rx_eci_channels(4)      => link_eci_packet_rx.dcs_c6,
    rx_eci_channels(5)      => link_eci_packet_rx.dcs_c11,
    rx_eci_channels(6)      => link_eci_packet_rx.dcs_c10,
    rx_eci_channels(7)      => link_eci_packet_rx.dcs_c5,
    rx_eci_channels(8)      => link_eci_packet_rx.dcs_c4,

    rx_eci_channels_ready(0)   => link_eci_packet_rx.c7_gsync_ready,
    rx_eci_channels_ready(1)   => link_eci_packet_rx.c6_gsync_ready,
    rx_eci_channels_ready(2)   => '1',
    rx_eci_channels_ready(3)   => link_eci_packet_rx.dcs_c7_ready,
    rx_eci_channels_ready(4)   => link_eci_packet_rx.dcs_c6_ready,
    rx_eci_channels_ready(5)   => link_eci_packet_rx.dcs_c11_ready,
    rx_eci_channels_ready(6)   => link_eci_packet_rx.dcs_c10_ready,
    rx_eci_channels_ready(7)   => link_eci_packet_rx.dcs_c5_ready,
    rx_eci_channels_ready(8)   => link_eci_packet_rx.dcs_c4_ready,

    tx_eci_channels(0)      => link_eci_packet_tx.c11_gsync,
    tx_eci_channels(1)      => link_eci_packet_tx.c10_gsync,
    tx_eci_channels(2)      => link_eci_packet_tx.dcs_c11,
    tx_eci_channels(3)      => link_eci_packet_tx.dcs_c5,
    tx_eci_channels(4)      => link_eci_packet_tx.dcs_c10,
    tx_eci_channels(5)      => link_eci_packet_tx.dcs_c4,
    tx_eci_channels(6)      => link_eci_packet_tx.dcs_c8,
    tx_eci_channels(7)      => link_eci_packet_tx.dcs_c9,

    tx_eci_channels_ready(0)   => link_eci_packet_tx.c11_gsync_ready,
    tx_eci_channels_ready(1)   => link_eci_packet_tx.c10_gsync_ready,
    tx_eci_channels_ready(2)   => link_eci_packet_tx.dcs_c11_ready,
    tx_eci_channels_ready(3)   => link_eci_packet_tx.dcs_c5_ready,
    tx_eci_channels_ready(4)   => link_eci_packet_tx.dcs_c10_ready,
    tx_eci_channels_ready(5)   => link_eci_packet_tx.dcs_c4_ready,
    tx_eci_channels_ready(6)   => link_eci_packet_tx.dcs_c8_ready,
    tx_eci_channels_ready(7)   => link_eci_packet_tx.dcs_c9_ready
);

-- GSYNC response handler, sends GSDN.
-- Odd VCs, GSYNC arrives in VC7 and GSDN sent in VC11.
vc7_vc11_gsync_loopback : loopback_vc_resp_nodata
generic map (
   WORD_WIDTH => 64,
   GSDN_GSYNC_FN => 1
)
port map (
    clk   => clk,
    reset => reset,

    vc_req_i       => link_eci_packet_rx.c7_gsync.data(0),
    vc_req_valid_i => link_eci_packet_rx.c7_gsync.valid,
    vc_req_ready_o => link_eci_packet_rx.c7_gsync_ready,

    vc_resp_o       => link_eci_packet_tx.c11_gsync.data(0),
    vc_resp_valid_o => link_eci_packet_tx.c11_gsync.valid,
    vc_resp_ready_i => link_eci_packet_tx.c11_gsync_ready
);

link_eci_packet_tx.c11_gsync.vc_no <= "1011";
link_eci_packet_tx.c11_gsync.size <= "000";

-- GSYNC response handler, sends GSDN.
-- Even VCs, GSYNC arrives in VC6 and GSDN sent in VC10.
vc6_vc10_gsync_loopback : loopback_vc_resp_nodata
generic map (
   WORD_WIDTH => 64,
   GSDN_GSYNC_FN => 1
)
port map (
    clk   => clk,
    reset => reset,

    vc_req_i       => link_eci_packet_rx.c6_gsync.data(0),
    vc_req_valid_i => link_eci_packet_rx.c6_gsync.valid,
    vc_req_ready_o => link_eci_packet_rx.c6_gsync_ready,

    vc_resp_o       => link_eci_packet_tx.c10_gsync.data(0),
    vc_resp_valid_o => link_eci_packet_tx.c10_gsync.valid,
    vc_resp_ready_i => link_eci_packet_tx.c10_gsync_ready
);

link_eci_packet_tx.c10_gsync.vc_no <= "1010";
link_eci_packet_tx.c10_gsync.size <= "000";

-- RX packetizer.
-- Packetize data from eci_gateway into ECI packet. 
-- RX rsp_wd VC5.
i_dcs_c5_eci_channel_to_bus : eci_channel_bus_converter
port map (
    clk             => clk,
    -- Input eci_gateway packet.
    in_channel      => link_eci_packet_rx.dcs_c5,
    in_ready        => link_eci_packet_rx.dcs_c5_ready,
    -- Output ECI packet.
    out_data        => link_eci_packet_rx.dcs_c5_wd_pkt,
    out_vc_no       => link_eci_packet_rx.dcs_c5_wd_pkt_vc,
    out_size        => link_eci_packet_rx.dcs_c5_wd_pkt_size,
    out_valid       => link_eci_packet_rx.dcs_c5_wd_pkt_valid,
    out_ready       => link_eci_packet_rx.dcs_c5_wd_pkt_ready
);

-- TX serializer.
-- Serialize ECI packet into eci_gateway.
-- TX rsp_wd VC5.
i_dcs_c5_bus_to_eci_channel : eci_bus_channel_converter
port map (
    clk             => clk,
    -- Input ECI packet.
    in_data         => vector_to_words(link_eci_packet_tx.dcs_c5_wd_pkt),
    in_vc_no        => link_eci_packet_tx.dcs_c5_wd_pkt_vc,
    in_size         => link_eci_packet_tx.dcs_c5_wd_pkt_size,
    in_valid        => link_eci_packet_tx.dcs_c5_wd_pkt_valid,
    in_ready        => link_eci_packet_tx.dcs_c5_wd_pkt_ready,
    -- output eci_gateway packet.
    out_channel     => link_eci_packet_tx.dcs_c5,
    out_ready       => link_eci_packet_tx.dcs_c5_ready
);

-- RX packetizer.
-- Packetize data from eci_gateway into ECI packet. 
-- RX rsp_wd VC4.
i_dcs_c4_eci_channel_to_bus : eci_channel_bus_converter
port map (
    clk             => clk,
    -- Input eci_gateway packet.
    in_channel      => link_eci_packet_rx.dcs_c4,
    in_ready        => link_eci_packet_rx.dcs_c4_ready,
    -- output ECI packet.
    out_data        => link_eci_packet_rx.dcs_c4_wd_pkt,
    out_vc_no       => link_eci_packet_rx.dcs_c4_wd_pkt_vc,
    out_size        => link_eci_packet_rx.dcs_c4_wd_pkt_size,
    out_valid       => link_eci_packet_rx.dcs_c4_wd_pkt_valid,
    out_ready       => link_eci_packet_rx.dcs_c4_wd_pkt_ready
);

-- TX serializer.
-- Serialize ECI packet into eci_gateway.
-- TX rsp_wd VC4.
i_dcs_c4_bus_to_eci_channel : eci_bus_channel_converter
port map (
    clk             => clk,
    -- Input ECI packet.
    in_data         => vector_to_words(link_eci_packet_tx.dcs_c4_wd_pkt),
    in_vc_no        => link_eci_packet_tx.dcs_c4_wd_pkt_vc,
    in_size         => link_eci_packet_tx.dcs_c4_wd_pkt_size,
    in_valid        => link_eci_packet_tx.dcs_c4_wd_pkt_valid,
    in_ready        => link_eci_packet_tx.dcs_c4_wd_pkt_ready,
    -- output eci_gateway packet.
    out_channel     => link_eci_packet_tx.dcs_c4,
    out_ready       => link_eci_packet_tx.dcs_c4_ready
);

-- -- VIO to initialize FPGA threads and observe
-- -- Performance counters.
-- vio_0_inst : vio_0
-- port map (
--   clk => clk,
--   probe_in0  => do_f_rmw_done_o, -- 1 bit
--   probe_in1  => do_f_tot_timer_o, -- 38
--   probe_in2  => do_f_num_wr_issued_o, -- 38
--   probe_in3  => do_f_num_rd_issued_o, -- 38
--   probe_in4  => de_f_rmw_done_o, -- 1
--   probe_in5  => de_f_tot_timer_o, -- 38
--   probe_in6  => de_f_num_wr_issued_o, -- 38
--   probe_in7  => de_f_num_rd_issued_o, -- 38
--   probe_out0 => f_st_un_al_addr_i, -- 38
--   probe_out1 => f_num_cls_to_rmw_i, --38
--   probe_out2 => f_rmw_en_i  -- 1
-- );

-- AXI Lite configuration registers. 
ft_cnc_ctrl_inst : ft_cnc_ctrl
  port map (
    clk                  => clk,
    reset                => reset,
    s_io_axil_awaddr     => s_io_axil_awaddr,
    s_io_axil_awvalid    => s_io_axil_awvalid,
    s_io_axil_awready    => s_io_axil_awready,
    s_io_axil_wdata      => s_io_axil_wdata,
    s_io_axil_wstrb      => s_io_axil_wstrb,
    s_io_axil_wvalid     => s_io_axil_wvalid,
    s_io_axil_wready     => s_io_axil_wready,
    s_io_axil_bresp      => s_io_axil_bresp,
    s_io_axil_bvalid     => s_io_axil_bvalid,
    s_io_axil_bready     => s_io_axil_bready,
    s_io_axil_araddr     => s_io_axil_araddr,
    s_io_axil_arvalid    => s_io_axil_arvalid,
    s_io_axil_arready    => s_io_axil_arready,
    s_io_axil_rdata      => s_io_axil_rdata,
    s_io_axil_rresp      => s_io_axil_rresp,
    s_io_axil_rvalid     => s_io_axil_rvalid,
    s_io_axil_rready     => s_io_axil_rready,
    st_un_al_addr_o      => f_st_un_al_addr_i,
    num_cls_to_rmw_o     => f_num_cls_to_rmw_i,
    rmw_en_o             => f_rmw_en_i,
    odd_tot_timer_i      => do_f_tot_timer_o,
    odd_num_wr_issued_i  => do_f_num_wr_issued_o,
    odd_num_rd_issued_i  => do_f_num_rd_issued_o,
    even_tot_timer_i     => de_f_tot_timer_o,
    even_num_wr_issued_i => de_f_num_wr_issued_o,
    even_num_rd_issued_i => de_f_num_rd_issued_o
    );

-- DC Slices: One DCS for odd and another for even VCs.
-- DCS for even VCs ie odd CL indices. 
dcs_even : dcs_2_axi
generic map (
  FPGA_THREAD_ODD_GEN => '1'
)
port map (
  clk   => clk,
  reset => reset,

  -- Input ECI events.
  -- ECI packet for request without data. (VC 6 or 7) (only header).
  req_wod_hdr_i       => link_eci_packet_rx.dcs_c6.data(0),
  req_wod_pkt_size_i  => "00001",
  req_wod_pkt_vc_i    => link_eci_packet_rx.dcs_c6.vc_no,
  req_wod_pkt_valid_i => link_eci_packet_rx.dcs_c6.valid,
  req_wod_pkt_ready_o => link_eci_packet_rx.dcs_c6_ready,

  -- ECI packet for response without data.(VC 10 or 11). (only header).
  rsp_wod_hdr_i       => link_eci_packet_rx.dcs_c10.data(0),
  rsp_wod_pkt_size_i  => "00001",
  rsp_wod_pkt_vc_i    => link_eci_packet_rx.dcs_c10.vc_no,
  rsp_wod_pkt_valid_i => link_eci_packet_rx.dcs_c10.valid,
  rsp_wod_pkt_ready_o => link_eci_packet_rx.dcs_c10_ready,

  -- ECI packet for response with data. (VC 4 or 5). (header + data).
  rsp_wd_pkt_i        => words_to_vector(link_eci_packet_rx.dcs_c4_wd_pkt),
  rsp_wd_pkt_size_i   => link_eci_packet_rx.dcs_c4_wd_pkt_size,
  rsp_wd_pkt_vc_i     => link_eci_packet_rx.dcs_c4_wd_pkt_vc,
  rsp_wd_pkt_valid_i  => link_eci_packet_rx.dcs_c4_wd_pkt_valid,
  rsp_wd_pkt_ready_o  => link_eci_packet_rx.dcs_c4_wd_pkt_ready,
  
  -- Output ECI events. (rsp without data, rsp with data).
  -- VC 10,11
  rsp_wod_hdr_o                  => link_eci_packet_tx.dcs_c10.data(0),
  rsp_wod_pkt_size_o             => open,
  rsp_wod_pkt_vc_o               => link_eci_packet_tx.dcs_c10.vc_no,
  rsp_wod_pkt_valid_o            => link_eci_packet_tx.dcs_c10.valid,
  rsp_wod_pkt_ready_i            => link_eci_packet_tx.dcs_c10_ready,

  -- Responses with data (VC 5 or 4)
  -- header+payload
  rsp_wd_pkt_o                   => link_eci_packet_tx.dcs_c4_wd_pkt,
  rsp_wd_pkt_size_o              => link_eci_packet_tx.dcs_c4_wd_pkt_size,
  rsp_wd_pkt_vc_o                => link_eci_packet_tx.dcs_c4_wd_pkt_vc,
  rsp_wd_pkt_valid_o             => link_eci_packet_tx.dcs_c4_wd_pkt_valid,
  rsp_wd_pkt_ready_i             => link_eci_packet_tx.dcs_c4_wd_pkt_ready,

  -- ECI fwd without data (VC 8 or 9)
  fwd_wod_hdr_o       => link_eci_packet_tx.dcs_c8.data(0), 
  fwd_wod_pkt_size_o  => open,
  fwd_wod_pkt_vc_o    => link_eci_packet_tx.dcs_c8.vc_no,
  fwd_wod_pkt_valid_o => link_eci_packet_tx.dcs_c8.valid,
  fwd_wod_pkt_ready_i => link_eci_packet_tx.dcs_c8_ready,

  -- Init FPGA thread.
  f_st_un_al_addr_i  => f_st_un_al_addr_i,
  f_num_cls_to_rmw_i => f_num_cls_to_rmw_i,
  f_rmw_en_i         => f_rmw_en_i,
  f_rmw_done_o       => do_f_rmw_done_o(0),
  f_tot_timer_o      => do_f_tot_timer_o,
  f_num_wr_issued_o  => do_f_num_wr_issued_o,
  f_num_rd_issued_o  => do_f_num_rd_issued_o,
    
  -- Primary AXI rd/wr i/f.
  p_axi_arid    => dcs_even_axi.arid,
  p_axi_araddr  => dcs_even_axi.araddr,
  p_axi_arlen   => dcs_even_axi.arlen,
  p_axi_arsize  => dcs_even_axi.arsize,
  p_axi_arburst => dcs_even_axi.arburst,
  p_axi_arlock  => dcs_even_axi.arlock,
  p_axi_arcache => dcs_even_axi.arcache,
  p_axi_arprot  => dcs_even_axi.arprot,
  p_axi_arvalid => dcs_even_axi.arvalid,
  p_axi_arready => dcs_even_axi.arready,
  p_axi_rid     => dcs_even_axi.rid,
  p_axi_rdata   => dcs_even_axi.rdata,
  p_axi_rresp   => dcs_even_axi.rresp,
  p_axi_rlast   => dcs_even_axi.rlast,
  p_axi_rvalid  => dcs_even_axi.rvalid,
  p_axi_rready  => dcs_even_axi.rready,

  p_axi_awid    => dcs_even_axi.awid,
  p_axi_awaddr  => dcs_even_axi.awaddr,
  p_axi_awlen   => dcs_even_axi.awlen,
  p_axi_awsize  => dcs_even_axi.awsize,
  p_axi_awburst => dcs_even_axi.awburst,
  p_axi_awlock  => dcs_even_axi.awlock,
  p_axi_awcache => dcs_even_axi.awcache,
  p_axi_awprot  => dcs_even_axi.awprot,
  p_axi_awvalid => dcs_even_axi.awvalid,
  p_axi_awready => dcs_even_axi.awready,
  p_axi_wdata   => dcs_even_axi.wdata,
  p_axi_wstrb   => dcs_even_axi.wstrb,
  p_axi_wlast   => dcs_even_axi.wlast,
  p_axi_wvalid  => dcs_even_axi.wvalid,
  p_axi_wready  => dcs_even_axi.wready,
  p_axi_bid     => dcs_even_axi.bid,
  p_axi_bresp   => dcs_even_axi.bresp,
  p_axi_bvalid  => dcs_even_axi.bvalid,
  p_axi_bready  => dcs_even_axi.bready
  );

-- DCS for odd VCs ie even CL indices. 
dcs_odd : dcs_2_axi
generic map (
  FPGA_THREAD_ODD_GEN => '0'
)
port map (
  clk   => clk,
  reset => reset,

  -- Input ECI events.
  -- ECI packet for request without data. (VC 6 or 7) (only header).
  req_wod_hdr_i       => link_eci_packet_rx.dcs_c7.data(0),
  req_wod_pkt_size_i  => "00001",
  req_wod_pkt_vc_i    => link_eci_packet_rx.dcs_c7.vc_no,
  req_wod_pkt_valid_i => link_eci_packet_rx.dcs_c7.valid,
  req_wod_pkt_ready_o => link_eci_packet_rx.dcs_c7_ready,

  -- ECI packet for response without data.(VC 10 or 11). (only header).
  rsp_wod_hdr_i       => link_eci_packet_rx.dcs_c11.data(0),
  rsp_wod_pkt_size_i  => "00001",
  rsp_wod_pkt_vc_i    => link_eci_packet_rx.dcs_c11.vc_no,
  rsp_wod_pkt_valid_i => link_eci_packet_rx.dcs_c11.valid,
  rsp_wod_pkt_ready_o => link_eci_packet_rx.dcs_c11_ready,

  -- ECI packet for response with data. (VC 4 or 5). (header + data).
  rsp_wd_pkt_i        => words_to_vector(link_eci_packet_rx.dcs_c5_wd_pkt),
  rsp_wd_pkt_size_i   => link_eci_packet_rx.dcs_c5_wd_pkt_size,
  rsp_wd_pkt_vc_i     => link_eci_packet_rx.dcs_c5_wd_pkt_vc,
  rsp_wd_pkt_valid_i  => link_eci_packet_rx.dcs_c5_wd_pkt_valid,
  rsp_wd_pkt_ready_o  => link_eci_packet_rx.dcs_c5_wd_pkt_ready,

  -- Output ECI events. (rsp without data, rsp with data).
  -- VC 10,11
  rsp_wod_hdr_o                  => link_eci_packet_tx.dcs_c11.data(0),
  rsp_wod_pkt_size_o             => open,
  rsp_wod_pkt_vc_o               => link_eci_packet_tx.dcs_c11.vc_no,
  rsp_wod_pkt_valid_o            => link_eci_packet_tx.dcs_c11.valid,
  rsp_wod_pkt_ready_i            => link_eci_packet_tx.dcs_c11_ready,

  -- Responses with data (VC 5 or 4)
  -- header+payload
  rsp_wd_pkt_o                   => link_eci_packet_tx.dcs_c5_wd_pkt,
  rsp_wd_pkt_size_o              => link_eci_packet_tx.dcs_c5_wd_pkt_size,
  rsp_wd_pkt_vc_o                => link_eci_packet_tx.dcs_c5_wd_pkt_vc,
  rsp_wd_pkt_valid_o             => link_eci_packet_tx.dcs_c5_wd_pkt_valid,
  rsp_wd_pkt_ready_i             => link_eci_packet_tx.dcs_c5_wd_pkt_ready,

  -- ECI fwd without data (VC 8 or 9)
  fwd_wod_hdr_o       => link_eci_packet_tx.dcs_c9.data(0), 
  fwd_wod_pkt_size_o  => open,
  fwd_wod_pkt_vc_o    => link_eci_packet_tx.dcs_c9.vc_no,
  fwd_wod_pkt_valid_o => link_eci_packet_tx.dcs_c9.valid,
  fwd_wod_pkt_ready_i => link_eci_packet_tx.dcs_c9_ready,

  -- Init FPGA thread.
  f_st_un_al_addr_i  => f_st_un_al_addr_i,
  f_num_cls_to_rmw_i => f_num_cls_to_rmw_i,
  f_rmw_en_i         => f_rmw_en_i,
  f_rmw_done_o       => de_f_rmw_done_o(0),
  f_tot_timer_o      => de_f_tot_timer_o,
  f_num_wr_issued_o  => de_f_num_wr_issued_o,
  f_num_rd_issued_o  => de_f_num_rd_issued_o,

  -- Primary AXI rd/wr i/f.
  p_axi_arid    => dcs_odd_axi.arid,
  p_axi_araddr  => dcs_odd_axi.araddr,
  p_axi_arlen   => dcs_odd_axi.arlen,
  p_axi_arsize  => dcs_odd_axi.arsize,
  p_axi_arburst => dcs_odd_axi.arburst,
  p_axi_arlock  => dcs_odd_axi.arlock,
  p_axi_arcache => dcs_odd_axi.arcache,
  p_axi_arprot  => dcs_odd_axi.arprot,
  p_axi_arvalid => dcs_odd_axi.arvalid,
  p_axi_arready => dcs_odd_axi.arready,
  p_axi_rid     => dcs_odd_axi.rid,
  p_axi_rdata   => dcs_odd_axi.rdata,
  p_axi_rresp   => dcs_odd_axi.rresp,
  p_axi_rlast   => dcs_odd_axi.rlast,
  p_axi_rvalid  => dcs_odd_axi.rvalid,
  p_axi_rready  => dcs_odd_axi.rready,

  p_axi_awid    => dcs_odd_axi.awid,
  p_axi_awaddr  => dcs_odd_axi.awaddr,
  p_axi_awlen   => dcs_odd_axi.awlen,
  p_axi_awsize  => dcs_odd_axi.awsize,
  p_axi_awburst => dcs_odd_axi.awburst,
  p_axi_awlock  => dcs_odd_axi.awlock,
  p_axi_awcache => dcs_odd_axi.awcache,
  p_axi_awprot  => dcs_odd_axi.awprot,
  p_axi_awvalid => dcs_odd_axi.awvalid,
  p_axi_awready => dcs_odd_axi.awready,
  p_axi_wdata   => dcs_odd_axi.wdata,
  p_axi_wstrb   => dcs_odd_axi.wstrb,
  p_axi_wlast   => dcs_odd_axi.wlast,
  p_axi_wvalid  => dcs_odd_axi.wvalid,
  p_axi_wready  => dcs_odd_axi.wready,
  p_axi_bid     => dcs_odd_axi.bid,
  p_axi_bresp   => dcs_odd_axi.bresp,
  p_axi_bvalid  => dcs_odd_axi.bvalid,
  p_axi_bready  => dcs_odd_axi.bready

);

G_DDR : if DDR_OR_BRAM = 1 generate
-- Connect AXI interfaces from DCS to DDR.
i_dcs_ddr: design_1
  port map(
    clk_sys => clk,
    reset_n => reset_n,
    ddr4_sys_rst => reset,
    -- DCS EVEN AXI
    ddr4_c1_axi_arid => dcs_even_axi.arid,
    ddr4_c1_axi_araddr => dcs_even_axi.araddr,
    ddr4_c1_axi_arlen => dcs_even_axi.arlen,
    ddr4_c1_axi_arsize => dcs_even_axi.arsize,
    ddr4_c1_axi_arburst => dcs_even_axi.arburst,
    ddr4_c1_axi_arlock(0) => dcs_even_axi.arlock,
    ddr4_c1_axi_arcache => dcs_even_axi.arcache,
    ddr4_c1_axi_arprot => dcs_even_axi.arprot,
    ddr4_c1_axi_arvalid => dcs_even_axi.arvalid,
    ddr4_c1_axi_arready => dcs_even_axi.arready,
    ddr4_c1_axi_arqos => (others => '0'),
    ddr4_c1_axi_arregion => (others => '0'),
    ddr4_c1_axi_rid => dcs_even_axi.rid,
    ddr4_c1_axi_rdata => dcs_even_axi.rdata,
    ddr4_c1_axi_rresp => dcs_even_axi.rresp,
    ddr4_c1_axi_rlast => dcs_even_axi.rlast,
    ddr4_c1_axi_rvalid => dcs_even_axi.rvalid,
    ddr4_c1_axi_rready => dcs_even_axi.rready,
    ddr4_c1_axi_awid => dcs_even_axi.awid,
    ddr4_c1_axi_awaddr => dcs_even_axi.awaddr,
    ddr4_c1_axi_awlen => dcs_even_axi.awlen,
    ddr4_c1_axi_awsize => dcs_even_axi.awsize,
    ddr4_c1_axi_awburst => dcs_even_axi.awburst,
    ddr4_c1_axi_awlock(0) => dcs_even_axi.awlock,
    ddr4_c1_axi_awcache => dcs_even_axi.awcache,
    ddr4_c1_axi_awprot => dcs_even_axi.awprot,
    ddr4_c1_axi_awvalid => dcs_even_axi.awvalid,
    ddr4_c1_axi_awready => dcs_even_axi.awready,
    ddr4_c1_axi_awqos => (others => '0'),
    ddr4_c1_axi_awregion => (others => '0'),
    ddr4_c1_axi_wdata => dcs_even_axi.wdata,
    ddr4_c1_axi_wstrb => dcs_even_axi.wstrb,
    ddr4_c1_axi_wlast => dcs_even_axi.wlast,
    ddr4_c1_axi_wvalid => dcs_even_axi.wvalid,
    ddr4_c1_axi_wready => dcs_even_axi.wready,
    ddr4_c1_axi_bid => dcs_even_axi.bid,
    ddr4_c1_axi_bresp => dcs_even_axi.bresp,
    ddr4_c1_axi_bvalid => dcs_even_axi.bvalid,
    ddr4_c1_axi_bready => dcs_even_axi.bready,

    -- DCS ODD AXI
    ddr4_c4_axi_arid => dcs_odd_axi.arid,
    ddr4_c4_axi_araddr => dcs_odd_axi.araddr,
    ddr4_c4_axi_arlen => dcs_odd_axi.arlen,
    ddr4_c4_axi_arsize => dcs_odd_axi.arsize,
    ddr4_c4_axi_arburst => dcs_odd_axi.arburst,
    ddr4_c4_axi_arlock(0) => dcs_odd_axi.arlock,
    ddr4_c4_axi_arcache => dcs_odd_axi.arcache,
    ddr4_c4_axi_arprot => dcs_odd_axi.arprot,
    ddr4_c4_axi_arvalid => dcs_odd_axi.arvalid,
    ddr4_c4_axi_arready => dcs_odd_axi.arready,
    ddr4_c4_axi_arqos => (others => '0'),
    ddr4_c4_axi_arregion => (others => '0'),
    ddr4_c4_axi_rid => dcs_odd_axi.rid,
    ddr4_c4_axi_rdata => dcs_odd_axi.rdata,
    ddr4_c4_axi_rresp => dcs_odd_axi.rresp,
    ddr4_c4_axi_rlast => dcs_odd_axi.rlast,
    ddr4_c4_axi_rvalid => dcs_odd_axi.rvalid,
    ddr4_c4_axi_rready => dcs_odd_axi.rready,
    ddr4_c4_axi_awid => dcs_odd_axi.awid,
    ddr4_c4_axi_awaddr => dcs_odd_axi.awaddr,
    ddr4_c4_axi_awlen => dcs_odd_axi.awlen,
    ddr4_c4_axi_awsize => dcs_odd_axi.awsize,
    ddr4_c4_axi_awburst => dcs_odd_axi.awburst,
    ddr4_c4_axi_awlock(0) => dcs_odd_axi.awlock,
    ddr4_c4_axi_awcache => dcs_odd_axi.awcache,
    ddr4_c4_axi_awprot => dcs_odd_axi.awprot,
    ddr4_c4_axi_awvalid => dcs_odd_axi.awvalid,
    ddr4_c4_axi_awready => dcs_odd_axi.awready,
    ddr4_c4_axi_awqos => (others => '0'),
    ddr4_c4_axi_awregion => (others => '0'),
    ddr4_c4_axi_wdata => dcs_odd_axi.wdata,
    ddr4_c4_axi_wstrb => dcs_odd_axi.wstrb,
    ddr4_c4_axi_wlast => dcs_odd_axi.wlast,
    ddr4_c4_axi_wvalid => dcs_odd_axi.wvalid,
    ddr4_c4_axi_wready => dcs_odd_axi.wready,
    ddr4_c4_axi_bid => dcs_odd_axi.bid,
    ddr4_c4_axi_bresp => dcs_odd_axi.bresp,
    ddr4_c4_axi_bvalid => dcs_odd_axi.bvalid,
    ddr4_c4_axi_bready => dcs_odd_axi.bready,

    -- C1 axil ctrl not connected.
    ddr4_c1_axi_ctrl_araddr => (others => '0'),
    ddr4_c1_axi_ctrl_arready => open,
    ddr4_c1_axi_ctrl_arvalid => '0',
    ddr4_c1_axi_ctrl_awaddr => (others => '0'),
    ddr4_c1_axi_ctrl_awready => open,
    ddr4_c1_axi_ctrl_awvalid => '0',
    ddr4_c1_axi_ctrl_bready => '0',
    ddr4_c1_axi_ctrl_bresp => open,
    ddr4_c1_axi_ctrl_bvalid => open,
    ddr4_c1_axi_ctrl_rdata => open,
    ddr4_c1_axi_ctrl_rready => '0',
    ddr4_c1_axi_ctrl_rresp => open,
    ddr4_c1_axi_ctrl_rvalid => open,
    ddr4_c1_axi_ctrl_wdata => (others => '0'),
    ddr4_c1_axi_ctrl_wready => open,
    ddr4_c1_axi_ctrl_wvalid => '0',

    -- C4 axil ctrl not connected.
    ddr4_c4_axi_ctrl_araddr => (others => '0'),
    ddr4_c4_axi_ctrl_arready => open,
    ddr4_c4_axi_ctrl_arvalid => '0',
    ddr4_c4_axi_ctrl_awaddr => (others => '0'),
    ddr4_c4_axi_ctrl_awready => open,
    ddr4_c4_axi_ctrl_awvalid => '0',
    ddr4_c4_axi_ctrl_bready => '0',
    ddr4_c4_axi_ctrl_bresp => open,
    ddr4_c4_axi_ctrl_bvalid => open,
    ddr4_c4_axi_ctrl_rdata => open,
    ddr4_c4_axi_ctrl_rready => '0',
    ddr4_c4_axi_ctrl_rresp => open,
    ddr4_c4_axi_ctrl_rvalid => open,
    ddr4_c4_axi_ctrl_wdata => (others => '0'),
    ddr4_c4_axi_ctrl_wready => open,
    ddr4_c4_axi_ctrl_wvalid => '0',

    ddr4_c1_act_n     => F_D1_ACT_N,
    ddr4_c1_adr       => F_D1_A(16 downto 0),
    ddr4_c1_ba        => F_D1_BA,
    ddr4_c1_bg        => F_D1_BG,
    ddr4_c1_ck_c(0)   => F_D1_CK_N(0),
    ddr4_c1_ck_t(0)   => F_D1_CK_P(0),
    ddr4_c1_cke(0)    => F_D1_CKE(0),
    ddr4_c1_cs_n(0)   => F_D1_CS_N(0),
    ddr4_c1_dq        => F_D1_DQ,
    ddr4_c1_dqs_c     => F_D1_DQS_N,
    ddr4_c1_dqs_t     => F_D1_DQS_P,
    ddr4_c1_odt(0)    => F_D1_ODT(0),
    ddr4_c1_par       => F_D1_PARITY_N,
    ddr4_c1_reset_n   => F_D1_RESET_N,
    ddr4_c1_sys_clk_n => F_D1C_CLK_N,
    ddr4_c1_sys_clk_p => F_D1C_CLK_P,
    ddr4_c1_ui_clk    => open,

    ddr4_c4_act_n     => F_D4_ACT_N,
    ddr4_c4_adr       => F_D4_A(16 downto 0),
    ddr4_c4_ba        => F_D4_BA,
    ddr4_c4_bg        => F_D4_BG,
    ddr4_c4_ck_c(0)   => F_D4_CK_N(0),
    ddr4_c4_ck_t(0)   => F_D4_CK_P(0),
    ddr4_c4_cke(0)    => F_D4_CKE(0),
    ddr4_c4_cs_n(0)   => F_D4_CS_N(0),
    ddr4_c4_dq        => F_D4_DQ,
    ddr4_c4_dqs_c     => F_D4_DQS_N,
    ddr4_c4_dqs_t     => F_D4_DQS_P,
    ddr4_c4_odt(0)    => F_D4_ODT(0),
    ddr4_c4_par       => F_D4_PARITY_N,
    ddr4_c4_reset_n   => F_D4_RESET_N,
    ddr4_c4_sys_clk_n => F_D4C_CLK_N,
    ddr4_c4_sys_clk_p => F_D4C_CLK_P,
    ddr4_c4_ui_clk    => open
    );
end generate G_DDR;

G_BRAM : if DDR_OR_BRAM = 0 generate
-- Connect AXI interfaces from each DC slice instance to a BRAM.
-- BRAM for even CL indices.
-- Only lower 16 bits of the address are connected.
i_even_dcs_bram: axi_bram_ctrl_0
port map(
    s_axi_aclk    => clk,
    s_axi_aresetn => reset_n,
    s_axi_awid    => dcs_even_axi.awid,
    s_axi_awaddr  => dcs_even_axi.awaddr(15 downto 0),
    s_axi_awlen   => dcs_even_axi.awlen,
    s_axi_awsize  => dcs_even_axi.awsize,
    s_axi_awburst => dcs_even_axi.awburst,
    s_axi_awlock  => dcs_even_axi.awlock,
    s_axi_awcache => dcs_even_axi.awcache,
    s_axi_awprot  => dcs_even_axi.awprot,
    s_axi_awvalid => dcs_even_axi.awvalid,
    s_axi_awready => dcs_even_axi.awready,
    s_axi_wdata   => dcs_even_axi.wdata,
    s_axi_wstrb   => dcs_even_axi.wstrb,
    s_axi_wlast   => dcs_even_axi.wlast,
    s_axi_wvalid  => dcs_even_axi.wvalid,
    s_axi_wready  => dcs_even_axi.wready,
    s_axi_bid     => dcs_even_axi.bid,
    s_axi_bresp   => dcs_even_axi.bresp,
    s_axi_bvalid  => dcs_even_axi.bvalid,
    s_axi_bready  => dcs_even_axi.bready,
    s_axi_arid    => dcs_even_axi.arid,
    s_axi_araddr  => dcs_even_axi.araddr(15 downto 0),
    s_axi_arlen   => dcs_even_axi.arlen,
    s_axi_arsize  => dcs_even_axi.arsize,
    s_axi_arburst => dcs_even_axi.arburst,
    s_axi_arlock  => dcs_even_axi.arlock,
    s_axi_arcache => dcs_even_axi.arcache,
    s_axi_arprot  => dcs_even_axi.arprot,
    s_axi_arvalid => dcs_even_axi.arvalid,
    s_axi_arready => dcs_even_axi.arready,
    s_axi_rid     => dcs_even_axi.rid,
    s_axi_rdata   => dcs_even_axi.rdata,
    s_axi_rresp   => dcs_even_axi.rresp,
    s_axi_rlast   => dcs_even_axi.rlast,
    s_axi_rvalid  => dcs_even_axi.rvalid,
    s_axi_rready  => dcs_even_axi.rready
);

-- -- BRAM for odd CL indices.
-- -- Only lower 16 bits of the address are connected.
i_odd_dcs_bram: axi_bram_ctrl_0
port map(
    s_axi_aclk    => clk,
    s_axi_aresetn => reset_n,
    s_axi_awid    => dcs_odd_axi.awid,
    s_axi_awaddr  => dcs_odd_axi.awaddr(15 downto 0),
    s_axi_awlen   => dcs_odd_axi.awlen,
    s_axi_awsize  => dcs_odd_axi.awsize,
    s_axi_awburst => dcs_odd_axi.awburst,
    s_axi_awlock  => dcs_odd_axi.awlock,
    s_axi_awcache => dcs_odd_axi.awcache,
    s_axi_awprot  => dcs_odd_axi.awprot,
    s_axi_awvalid => dcs_odd_axi.awvalid,
    s_axi_awready => dcs_odd_axi.awready,
    s_axi_wdata   => dcs_odd_axi.wdata,
    s_axi_wstrb   => dcs_odd_axi.wstrb,
    s_axi_wlast   => dcs_odd_axi.wlast,
    s_axi_wvalid  => dcs_odd_axi.wvalid,
    s_axi_wready  => dcs_odd_axi.wready,
    s_axi_bid     => dcs_odd_axi.bid,
    s_axi_bresp   => dcs_odd_axi.bresp,
    s_axi_bvalid  => dcs_odd_axi.bvalid,
    s_axi_bready  => dcs_odd_axi.bready,
    s_axi_arid    => dcs_odd_axi.arid,
    s_axi_araddr  => dcs_odd_axi.araddr(15 downto 0),
    s_axi_arlen   => dcs_odd_axi.arlen,
    s_axi_arsize  => dcs_odd_axi.arsize,
    s_axi_arburst => dcs_odd_axi.arburst,
    s_axi_arlock  => dcs_odd_axi.arlock,
    s_axi_arcache => dcs_odd_axi.arcache,
    s_axi_arprot  => dcs_odd_axi.arprot,
    s_axi_arvalid => dcs_odd_axi.arvalid,
    s_axi_arready => dcs_odd_axi.arready,
    s_axi_rid     => dcs_odd_axi.rid,
    s_axi_rdata   => dcs_odd_axi.rdata,
    s_axi_rresp   => dcs_odd_axi.rresp,
    s_axi_rlast   => dcs_odd_axi.rlast,
    s_axi_rvalid  => dcs_odd_axi.rvalid,
    s_axi_rready  => dcs_odd_axi.rready
);
end generate G_BRAM;

end Behavioral;
