#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/eci_cmd_defs.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/eci_cc_defs.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/eci_dcs_defs.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/eci_dirc_defs.sv \
      ../../enzian-optimized-dcu/dcs/dcs/testbench/word_addr_mem.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/wr_trmgr.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dcu_controller.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dcu_tsu.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/rd_trmgr.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/eci_trmgr.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/gen_out_header.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dc_to_vc_router.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/decode_eci_req.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/eci_cc_table.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dcu.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/axis_2_router.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/axis_comb_priority_enc.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/axis_comb_rr_arb.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dcs_rr_arb.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/map_ecid_to_wrd.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/arb_4_ecih.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/arb_3_ecih.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/arb_2_ecih.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dcu_top.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/axis_pipeline_stage.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/tag_state_ram.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dcs.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/ram_tdp.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dp_mem.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dp_wr_ser.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dp_gate.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/wr_data_path.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dp_gen_path.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dp_data_store.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/rd_data_path.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/dcs_dcus.sv \
      ../../enzian-optimized-dcu/dcs/dcs/rtl/axis_comb_router.sv \
      ../../enzian-optimized-dcu/desc_to_axi/axi_rd_cl/rtl/axi_rd_cl.sv \
      ../../enzian-optimized-dcu/desc_to_axi/axi_wr_cl/rtl/axi_wr_cl.sv \
      ../../fpga_thread_rmw/ft_cnc_rmw/ft_cnc_rmw_top/rtl/ft_cnc_rmw_top.sv \
      ../../fpga_thread_rmw/ft_cnc_rmw/ft_cnc_rmw_top/rtl/ft_dc_desc_arb.sv \
      ../../fpga_thread_rmw/ft_cnc_rmw/ft_cnc_rmw_top/rtl/f_rmw_thread.sv \
      ../../fpga_thread_rmw/ft_cnc_rmw/ft_cnc_rmw_top/rtl/gen_seq_al.sv \
      ../../fpga_thread_rmw/ft_cnc_rmw/ft_cnc_rmw_top/rtl/ft_cnc_rmw.sv \
      ../dcs/dcs_2_axi.sv \
      ../testbench/dcs_2_axiTb.sv

xelab -debug typical -incremental -L xpm worklib.dcs_2_axiTb worklib.glbl -s worklib.dcs_2_axiTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.dcs_2_axiTb 
xsim -gui worklib.dcs_2_axiTb 
