import eci_cmd_defs::*;
import eci_dcs_defs::*;

module dcs_2_axiTb();

   parameter AXI_ID_WIDTH = MAX_DCU_ID_WIDTH; 
   parameter AXI_ADDR_WIDTH = DS_ADDR_WIDTH;  
   parameter AXI_DATA_WIDTH = 512;
   parameter AXI_STRB_WIDTH = (AXI_DATA_WIDTH/8);
   parameter PERF_REGS_WIDTH = 32;
   parameter SYNTH_PERF_REGS = 1; 
   parameter logic FPGA_THREAD_ODD_GEN = 1'b1;

   //input output ports 
   //Input signals
   logic 	   clk;
   logic 	   reset;
   logic [ECI_WORD_WIDTH-1:0] req_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] req_wod_pkt_size_i;
   logic [3:0] 			     req_wod_pkt_vc_i;
   logic 			     req_wod_pkt_valid_i;
   logic [ECI_WORD_WIDTH-1:0] 	     rsp_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] rsp_wod_pkt_size_i;
   logic [3:0] 			     rsp_wod_pkt_vc_i;
   logic 			     rsp_wod_pkt_valid_i;
   logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] rsp_wd_pkt_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wd_pkt_size_i;
   logic [3:0] 					   rsp_wd_pkt_vc_i;
   logic 					   rsp_wd_pkt_valid_i;
   logic 					   rsp_wod_pkt_ready_i;
   logic 					   rsp_wd_pkt_ready_i;
   logic 					   fwd_wod_pkt_ready_i;
   logic [DS_ADDR_WIDTH-1:0] 			   f_st_un_al_addr_i;
   logic [DS_ADDR_WIDTH-1:0] 			   f_num_cls_to_rmw_i;
   logic 					   f_rmw_en_i;
   logic 					   p_axi_arready;
   logic [AXI_ID_WIDTH-1:0] 			   p_axi_rid;
   logic [AXI_DATA_WIDTH-1:0] 			   p_axi_rdata;
   logic [1:0] 					   p_axi_rresp;
   logic 					   p_axi_rlast;
   logic 					   p_axi_rvalid;
   logic 					   p_axi_awready;
   logic 					   p_axi_wready;
   logic [AXI_ID_WIDTH-1:0] 			   p_axi_bid;
   logic [1:0] 					   p_axi_bresp;
   logic 					   p_axi_bvalid;

   //Output signals
   logic 					   req_wod_pkt_ready_o;
   logic 					   rsp_wod_pkt_ready_o;
   logic 					   rsp_wd_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 			   rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wod_pkt_size_o;
   logic [3:0] 					   rsp_wod_pkt_vc_o;
   logic 					   rsp_wod_pkt_valid_o;
   logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] rsp_wd_pkt_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   rsp_wd_pkt_size_o;
   logic [3:0] 					   rsp_wd_pkt_vc_o;
   logic 					   rsp_wd_pkt_valid_o;
   logic [ECI_WORD_WIDTH-1:0] 			   fwd_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		   fwd_wod_pkt_size_o;
   logic [3:0] 					   fwd_wod_pkt_vc_o;
   logic 					   fwd_wod_pkt_valid_o;
   logic 					   f_rmw_done_o;
   logic [DS_ADDR_WIDTH-1:0] 			   f_tot_timer_o;
   logic [DS_ADDR_WIDTH-1:0] 			   f_num_wr_issued_o;
   logic [DS_ADDR_WIDTH-1:0] 			   f_num_rd_issued_o;
   logic [AXI_ID_WIDTH-1:0] 			   p_axi_arid;
   logic [AXI_ADDR_WIDTH-1:0] 			   p_axi_araddr;
   logic [7:0] 					   p_axi_arlen;
   logic [2:0] 					   p_axi_arsize;
   logic [1:0] 					   p_axi_arburst;
   logic 					   p_axi_arlock;
   logic [3:0] 					   p_axi_arcache;
   logic [2:0] 					   p_axi_arprot;
   logic 					   p_axi_arvalid;
   logic 					   p_axi_rready;
   logic [AXI_ID_WIDTH-1:0] 			   p_axi_awid;
   logic [AXI_ADDR_WIDTH-1:0] 			   p_axi_awaddr;
   logic [7:0] 					   p_axi_awlen;
   logic [2:0] 					   p_axi_awsize;
   logic [1:0] 					   p_axi_awburst;
   logic 					   p_axi_awlock;
   logic [3:0] 					   p_axi_awcache;
   logic [2:0] 					   p_axi_awprot;
   logic 					   p_axi_awvalid;
   logic [AXI_DATA_WIDTH-1:0] 			   p_axi_wdata;
   logic [AXI_STRB_WIDTH-1:0] 			   p_axi_wstrb;
   logic 					   p_axi_wlast;
   logic 					   p_axi_wvalid;
   logic 					   p_axi_bready;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   dcs_2_axi dcs_2_axi1 (
			 .clk			(clk),
			 .reset			(reset),
			 .req_wod_hdr_i		(req_wod_hdr_i),
			 .req_wod_pkt_size_i	(req_wod_pkt_size_i),
			 .req_wod_pkt_vc_i	(req_wod_pkt_vc_i),
			 .req_wod_pkt_valid_i	(req_wod_pkt_valid_i),
			 .req_wod_pkt_ready_o	(req_wod_pkt_ready_o),
			 .rsp_wod_hdr_i		(rsp_wod_hdr_i),
			 .rsp_wod_pkt_size_i	(rsp_wod_pkt_size_i),
			 .rsp_wod_pkt_vc_i	(rsp_wod_pkt_vc_i),
			 .rsp_wod_pkt_valid_i	(rsp_wod_pkt_valid_i),
			 .rsp_wod_pkt_ready_o	(rsp_wod_pkt_ready_o),
			 .rsp_wd_pkt_i		(rsp_wd_pkt_i),
			 .rsp_wd_pkt_size_i	(rsp_wd_pkt_size_i),
			 .rsp_wd_pkt_vc_i	(rsp_wd_pkt_vc_i),
			 .rsp_wd_pkt_valid_i	(rsp_wd_pkt_valid_i),
			 .rsp_wd_pkt_ready_o	(rsp_wd_pkt_ready_o),
			 .rsp_wod_hdr_o		(rsp_wod_hdr_o),
			 .rsp_wod_pkt_size_o	(rsp_wod_pkt_size_o),
			 .rsp_wod_pkt_vc_o	(rsp_wod_pkt_vc_o),
			 .rsp_wod_pkt_valid_o	(rsp_wod_pkt_valid_o),
			 .rsp_wod_pkt_ready_i	(rsp_wod_pkt_ready_i),
			 .rsp_wd_pkt_o		(rsp_wd_pkt_o),
			 .rsp_wd_pkt_size_o	(rsp_wd_pkt_size_o),
			 .rsp_wd_pkt_vc_o	(rsp_wd_pkt_vc_o),
			 .rsp_wd_pkt_valid_o	(rsp_wd_pkt_valid_o),
			 .rsp_wd_pkt_ready_i	(rsp_wd_pkt_ready_i),
			 .fwd_wod_hdr_o		(fwd_wod_hdr_o),
			 .fwd_wod_pkt_size_o	(fwd_wod_pkt_size_o),
			 .fwd_wod_pkt_vc_o	(fwd_wod_pkt_vc_o),
			 .fwd_wod_pkt_valid_o	(fwd_wod_pkt_valid_o),
			 .fwd_wod_pkt_ready_i	(fwd_wod_pkt_ready_i),
			 .f_st_un_al_addr_i	(f_st_un_al_addr_i),
			 .f_num_cls_to_rmw_i	(f_num_cls_to_rmw_i),
			 .f_rmw_en_i		(f_rmw_en_i),
			 .f_rmw_done_o		(f_rmw_done_o),
			 .f_tot_timer_o		(f_tot_timer_o),
			 .f_num_wr_issued_o	(f_num_wr_issued_o),
			 .f_num_rd_issued_o	(f_num_rd_issued_o),
			 .p_axi_arid		(p_axi_arid),
			 .p_axi_araddr		(p_axi_araddr),
			 .p_axi_arlen		(p_axi_arlen),
			 .p_axi_arsize		(p_axi_arsize),
			 .p_axi_arburst		(p_axi_arburst),
			 .p_axi_arlock		(p_axi_arlock),
			 .p_axi_arcache		(p_axi_arcache),
			 .p_axi_arprot		(p_axi_arprot),
			 .p_axi_arvalid		(p_axi_arvalid),
			 .p_axi_arready		(p_axi_arready),
			 .p_axi_rid		(p_axi_rid),
			 .p_axi_rdata		(p_axi_rdata),
			 .p_axi_rresp		(p_axi_rresp),
			 .p_axi_rlast		(p_axi_rlast),
			 .p_axi_rvalid		(p_axi_rvalid),
			 .p_axi_rready		(p_axi_rready),
			 .p_axi_awid		(p_axi_awid),
			 .p_axi_awaddr		(p_axi_awaddr),
			 .p_axi_awlen		(p_axi_awlen),
			 .p_axi_awsize		(p_axi_awsize),
			 .p_axi_awburst		(p_axi_awburst),
			 .p_axi_awlock		(p_axi_awlock),
			 .p_axi_awcache		(p_axi_awcache),
			 .p_axi_awprot		(p_axi_awprot),
			 .p_axi_awvalid		(p_axi_awvalid),
			 .p_axi_awready		(p_axi_awready),
			 .p_axi_wdata		(p_axi_wdata),
			 .p_axi_wstrb		(p_axi_wstrb),
			 .p_axi_wlast		(p_axi_wlast),
			 .p_axi_wvalid		(p_axi_wvalid),
			 .p_axi_wready		(p_axi_wready),
			 .p_axi_bid		(p_axi_bid),
			 .p_axi_bresp		(p_axi_bresp),
			 .p_axi_bvalid		(p_axi_bvalid),
			 .p_axi_bready		(p_axi_bready)			 
			 );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      req_wod_hdr_i = '0;
      req_wod_pkt_size_i = '0;
      req_wod_pkt_vc_i = '0;
      req_wod_pkt_valid_i = '0;
      rsp_wod_hdr_i = '0;
      rsp_wod_pkt_size_i = '0;
      rsp_wod_pkt_vc_i = '0;
      rsp_wod_pkt_valid_i = '0;
      rsp_wd_pkt_i = '0;
      rsp_wd_pkt_size_i = '0;
      rsp_wd_pkt_vc_i = '0;
      rsp_wd_pkt_valid_i = '0;
      rsp_wod_pkt_ready_i = '0;
      rsp_wd_pkt_ready_i = '0;
      fwd_wod_pkt_ready_i = '0;
      f_st_un_al_addr_i = '0;
      f_num_cls_to_rmw_i = '0;
      f_rmw_en_i = '0;
      p_axi_arready = '0;
      p_axi_rid = '0;
      p_axi_rdata = '0;
      p_axi_rresp = '0;
      p_axi_rlast = '0;
      p_axi_rvalid = '0;
      p_axi_awready = '0;
      p_axi_wready = '0;
      p_axi_bid = '0;
      p_axi_bresp = '0;
      p_axi_bvalid = '0;

      ##5;
      reset = 1'b0;
      ##5;
      f_st_un_al_addr_i = '0;
      f_num_cls_to_rmw_i = 'd4;
      f_rmw_en_i = '1;
      p_axi_arready = '1;
      wait(p_axi_arvalid & p_axi_arready);
      ##1;
      #500 $finish;
   end // initial begin

endmodule
