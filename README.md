# Enzian App Coherent Table

Table on FPGA memory coherently read modified and written to by
threads on the CPU and FPGA.

## Init submodules.
git submodule update --init --recursive 